
const customeAccountInfos = [
  {
    id: 1,
    lastName: 'Doe', 
    'firstName': 'John', 
    amount: 1000, 
    created_Date: "09-01-2012",
    operations: []
  }
];

// afficher le compte du client
const displayCustomerAccountInfos = (customerId) => customeAccountInfos[customerId - 1];

// deposit
// pour ajouter de l'argent
const depositMoney = (amountToAdd, customerId = 1) => {
  if (amountToAdd <= 0) {
    return;
  }

  const customer = displayCustomerAccountInfos(customerId);
  customer.amount += amountToAdd;
  customer.operations = [...customer.operations, {operation: "deposit", add: amountToAdd, delete: 0, amount: customer.amount, date : '14-01-2012'}];
  return customer.amount;
}

// withdrawal
// pour retirer de l'argent
const withdrawal = (amountToWithdraw, customerId = 1) => {
  if (amountToWithdraw <= 0) {
    return;
  }
  const date = new Date();
  const customer = displayCustomerAccountInfos(customerId);
  customer.amount -= amountToWithdraw;
  customer.operations = [...customer.operations, {operation: "deposit", add: 0, delete: amountToWithdraw, amount: customer.amount, date}];
  return customer.amount;
}

// historic
// afficher l'historique du client
const displayHistoric = (customerId = 1) => {
  const customer = displayCustomerAccountInfos(customerId);

  console.log(customer.operations)
}

//displayHistoric();

module.exports = {
  displayHistoric,
  displayCustomerAccountInfos,
  customeAccountInfos,
  depositMoney,
  withdrawal
}